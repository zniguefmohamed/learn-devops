### chown : change ownership

    (sudo) chown <username>:<groupname> <filename>


### chgrp : change group

    (sudo) chgrp groupName fileName


<div>
  <img src="./images/photo_2023-06-12_11-48-09.jpg" width="450" >
</div>

<br />

<div>
  <img src="./images/Screenshot%202023-06-12%20120735.png" width="450" >
</div>

<br />

## Modifying permissions 

<br />


### Remove execute permission
    
    sudo chmod -x folderName
    

###  change group permission 
    (sudo) chmod g-w fileName

### add group permission
    (sudo) chmod g+x fileName

### give user permission to execute file

    (sudo) chmod u+x fileName

### Give permission to other

    (sudo) chmod o+x fileName

### You can change permissions by <br />

    u: user
    g: group
    o: other

    + add
    - remove
