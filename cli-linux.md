## Linux CLI

<br />

### `$ ==> Represent regular user`

<br />

## Sign for root user

    ls: List files & directories

    ls -a : display hidden files

    ls -l : shows us permissions of files

    cd: Change directory

    pwd: Print working directory

    mkdir: Create a new directory

    rmdir: Delete direcoty

    rm: Remove files and directories

    cp: Copy files and directories

    mv: Move or rename files and directories

    cat: Concatenate and display file content

    grep: Search for a pattern in files

    sudo: Execute a command with administrative privileges

    chmod: Change file permissions

    chown: Change file ownership

    tar: Create or extract compressed archives

    ssh: Securely access remote servers

    wget: Download files from the web

    top: Monitor system processes

    ps: Display information about running processes

    kill: Terminate processes

    man: Display the manual pages for a command

    history: View command history

    hostname -i : retrieve the IP address

    touch : create file ==> touch fileName

    install ==> sudo apt install 'package-name | app-name'

    su - ==> switch account
